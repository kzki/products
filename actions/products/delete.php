<?php
/**
 * Delete album or image, video, etc..
 *
 *
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License
 */

$guid = (int) get_input('guid');
$entity = get_entity($guid);
if (!$entity) {
    // unable to get Elgg entity
    register_error(elgg_echo("products:deletefailed"));
    forward(REFERER);
}

if (!$entity->canEdit()) {
    // user doesn't have permissions
    register_error(elgg_echo("products:deletefailed"));
    forward(REFERER);
}

$container = $entity->getContainerEntity();

$subtype = $entity->getSubtype();
switch($subtype) {
    case 'album':
        if (elgg_instanceof($container, 'user')) {
            $forward_url = "products/owner/$container->username";
        } else {
            $forward_url = "products/group/$container->guid/all";
        }
        break;
    case 'image':
        $forward_url = $container->getURL();
        break;
    case 'video':
    case 'music':
    case 'prog':
    case 'other':
    default:
        forward(REFERER);
        break;
}

if($entity->delete()) {
    system_message(elgg_echo("products:deleted"));
} else {
    register_error(elgg_echo("products:deletefaild"));
}

forward($forward_url);
