<?php
/**
 * Save video action
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

// Get input data
$url  = get_input('url');
$title = get_input('title');
$description = get_input('description');
$tags = get_input('tags');

elgg_make_sticky_form('products');

$guid = (int) get_input('guid');
$album = get_entity($guid);
if (!$album) {
	register_error(elgg_echo('products:baduploadform'));
	forward(REFERER);
}

if (empty($title)) {
	register_error(elgg_echo("video:blank"));
	forward(REFERER);
}

if (empty($url)) {
	register_error(elgg_echo("video:url:blank"));
	forward(REFERER);

}

$video = new ProductsVideo();

$video->url = $url;
$video->title = $title;
$video->description = $description;
$video->container_guid = $album->getGUID();
if ($tags) {
	$video->tags = string_to_tag_array($tags);
} else {
	$video->deleteMetadata('tags');
}

if (!$video->save()) {
	register_error(elgg_echo("video:error"));
	forward(REFERER);
}

elgg_clear_sticky_form('products');

system_message(elgg_echo("video:saved"));
forward($video->getURL());
