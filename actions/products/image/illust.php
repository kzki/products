<?php
/**
 * Multi-image uploader action
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

elgg_load_library('tidypics:upload');
$img_river_view = elgg_get_plugin_setting('img_river_view', 'products');

set_input('products_action_name', 'products_photo_upload');

$guid = (int) get_input('guid');
$album = get_entity($guid);
if (!$album) {
    register_error(elgg_echo('products:baduploadform'));
    forward(REFERER);
}

// post limit exceeded
if (count($_FILES) == 0) {
    trigger_error('Products warning: user exceeded post limit on image upload',E_USER_WARNING);
    register_error(elgg_echo('products:exceedpostlimit'));
    forward(REFERER);
}

// test to make sure at least 1 image was selected by user
$num_images = 0;
foreach($_FILES['images']['name'] as $name) {
    if(!empty($name)) {
        $num_images++;
    }
}
if ($num_images == 0) {
    // have user try again
    register_error(elgg_echo('products:noimages'));
    forward(REFERER);
}

// create the image object for each upload
$uploaded_images = array();
$not_uploaded = array();
$error_msgs = array();
foreach($_FILES['images']['name'] as $index => $value) {
    $data = array();
    foreach($_FILES['images'] as $key => $values) {
        $data[$key] = $values[$index];
    }

    if (empty($data['name'])) {
        continue;
    }

    $name = htmlspacialchars($data['name'], ENT_QUOTES, 'UTF-8', false);

    $mime = pd_upload_get_mimetype($name);


}
