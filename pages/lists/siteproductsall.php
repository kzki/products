<?php
/**
 * Most recently uploaded products
 *
 */

// set up breadcrumbs
elgg_push_breadcrumb(elgg_echo('products'), 'products/siteimagesall');

$offset = (int)get_input('offset', 0);
$limit = (int)get_input('limit', 16);

// grab the html to display the most recent products
$result = elgg_list_entities(array(
	'type' => 'object',
	'subtype' => array('image', 'video', 'music', 'prog', 'other'),
	'owner_guid' => NULL,
	'limit' => $limit,
	'offset' => $offset,
	'full_view' => false,
	'list_type' => 'gallery',
	'gallery_class' => 'products-gallery',
));

$title = elgg_echo('products:siteproductsall');

if (elgg_is_logged_in()) {
	$logged_in_guid = elgg_get_logged_in_user_guid();
	elgg_register_menu_item('title', array(
		'name' => 'addproducts',
		'href' => "ajax/view/products/selectalbum/?owner_guid=" . $logged_in_guid,
		'text' => elgg_echo("products:addproducts"),
		'link_class' => 'elgg-button elgg-button-action elgg-lightbox',
	));
}

// only show slidesho link if slideshow is enabled in plugin settings and there are images
//
//
//

if (!empty($result)) {
	$area2 = $result;
} else {
	$area2 = elgg_echo('products:siteproductsall:nosuccess');
}
$body = elgg_view_layout('content', array(
	'filter_override' => elgg_view('filter_override/siteproducts', array('selected' => 'all')),
	'content' => $area2,
	'title' => $title,
	'sidebar' => elgg_view('products/sidebar_im', array('page' => 'all')),
));

// Draw it
echo elgg_view_page($title, $body);
