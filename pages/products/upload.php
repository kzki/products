<?php
/**
 * Upload products
 *
 * @auther Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

elgg_gatekeeper();

$page = $vars['page'];

$album_guid = (int) get_input('guid');
if (!$album_guid) {
    // @todo
    forward();
}

$album = get_entity($album_guid);
if (!$album) {
    // @todo
    // throw warning and forward to previous page
    forward(REFERER);
}

if (!$album->getContainerEntity()->canWriteToContainer()) {
    // @todo have to be able to edit album to upload products
    forward(REFERER);
}

// set page owner based on container (user or group)
elgg_set_page_owner_guid($album->getContainerGUID());
$owner = elgg_get_page_owner_entity();
elgg_group_gatekeeper();

$title = elgg_echo('album:addproducts');

// set up breadcrumbs
elgg_push_breadcrumb(elgg_echo('products'), 'products/siteproductsall');
elgg_push_breadcrumb(elgg_echo('products:albums'), 'products/all');
elgg_push_breadcrumb($owner->name, "products/owner/$owner->username");
elgg_push_breadcrumb($album->getTitle(), $album->getURL());
elgg_push_breadcrumb(elgg_echo('album:addproducts'));

$vars = products_prepare_form_vars();
$content = "";
switch($page) {
    case 'illust':
        $content .= elgg_view('forms/products/upload/basic_upload', array('entity' => $album));
        break;
    case 'music':
        $content .= elgg_view_form('products/music/save', array('entity' => $album));
        break;
    case 'video':
        $content .= elgg_view('forms/products/video/save', array('entity' => $album));
        break;
    case 'program':
        $content .= elgg_view_form('products/program/save', array('method' => 'post'), $vars);
        break;
    case 'other':
        $content .= elgg_view_form('products/prog/save');
        break;
}

$body = elgg_view_layout('content', array(
    'filter_override' => elgg_view('filter_override/siteuploads', array('selected' => 'illust', 'album_guid' => $album_guid)),
    'content' => $content,
    'title' => $title,
    'sidebar' => elgg_view('products/sidebar_im', array('page' => 'upload')),
));

echo elgg_view_page($title, $body);
