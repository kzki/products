<?php
/**
 * View all albums on the site
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

elgg_push_breadcrumb(elgg_echo('products'), 'products/siteproductsall');
elgg_push_breadcrumb(elgg_echo('products:albums'));

$offset = (int)get_input('offset', 0);
$limit  = (int)get_input('limit', 16);

$content = elgg_list_entities(array(
    'type' => 'object',
    'subtype' => 'album',
    'limit' => $limit,
    'offset' => $offset,
    'full_view' => false,
    'list_type' => 'gallery',
    'list_type_toggle' => false,
    'gallery_class' => 'products-gallery',
));
if (!$content) {
    $content = elgg_echo('products:none');
}

$title = elgg_echo('album:all');

if (elgg_is_logged_in()) {
    $logged_in_guid = elgg_get_logged_in_user_guid();
    elgg_register_menu_item('title', array(
        'name' => 'addproducts',
        'href' => "ajax/view/products/selectalbum/?owner_guid=" . $logged_in_guid,
        'text' => elgg_echo("products:addproducts"),
        'link_class' => 'elgg-button elgg-button-action elgg-lightbox',
    ));
}

elgg_register_title_button('products');

$body = elgg_view_layout('content', array(
    'filter_context' => 'all',
    'content' => $content,
    'title' => $title,
    'sidebar' => elgg_view('products/sidebar_al', array('page' => 'all')),
));

echo elgg_view_page($title, $body);
