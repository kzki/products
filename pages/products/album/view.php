<?php
/**
 * This displays the products that belong to an album
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

// get the album entity
$album_guid = (int)get_input('guid');
$album = get_entity($album_guid);
if (!$album) {
    register_error(elgg_echo('noaccess'));
    $_SESSION['last_forward_from'] = current_page_url();
    forward('');
}
$container = $album->getContainerEntity();
if (!$container) {
    register_error(elgg_echo('noaccess'));
    $_SESSION['last_forward_from'] = current_page_url();
    forward('');
}

elgg_set_page_owner_guid($album->getContainerGUID());
$owner = elgg_get_page_owner_entity();
elgg_group_gatekeeper();

$title = elgg_echo($album->getTitle());

// set up breadcrumbs
elgg_push_breadcrumb(elgg_echo('products'), 'products/siteproductsall');
elgg_push_breadcrumb(elgg_echo('products:albums'), 'products/all');
if (elgg_instanceof($owner, 'group')) {
    elgg_push_breadcrumb($owner->name, "products/group/$owner->guid/all");
} else {
    elgg_push_breadcrumb($owner->name, "products/owner/$owner->username");
}
elgg_push_breadcrumb($title);

$content = elgg_view_entity($album, array('full_view' => true));

if (elgg_is_logged_in()) {
    if ($owner instanceof ElggGroup) {
        if ($owner->isMember(elgg_get_logged_in_user_entity())) {
            elgg_register_menu_item('title', array (
                'name' => 'addproducts',
                'href' => "ajax/view/products/selectalbum/?owner_guid=" . $onwer->getGUID(),
                'text' => elgg_echo("products:addproducts"),
                'link_class' => 'elgg-button elgg-button-action elgg-lightbox',
            ));
        }
    } else {
        elgg_register_menu_item('title', array(
            'name' => 'addproducts',
            'href' => "ajax/view/products/selectalbum/?owner_guid=" . elgg_get_logged_in_user_guid(),
            'text' => elgg_echo("products:addproducts"),
            'link_class' => 'elgg-button elgg-button-action elgg-lightbox',
        ));
    }
}

if ($album->getContainerEntity()->canWriteToContainer()) {
    elgg_register_menu_item('title', array(
        'name' => 'upload',
        'href' => 'products/upload/' . $album->getGUID(),
        'text' => elgg_echo('products:upload'),
        'link_class' => 'elgg-button elgg-button-action',
    ));
}

// only show sort button if there are products
if ($album->canEdit() && $album->getSize() > 0) {
    elgg_register_menu_item('title', array(
        'name' => 'sort',
        'href' => "products/sort/" . $album->getGUID(),
        'link_class' => 'elgg-button elgg-button-action',
        'priority' => 200,
    ));
}

// only show slideshow link if slideshow is enabled in plugin settings and there are products
//
//
//

$body = elgg_view_layout('content', array(
    'filter' => false,
    'content' => $content,
    'title' => $title,
    'sidebar' => elgg_view('products/sidebar_al', array('page' => 'album')),
));

echo elgg_view_page($title, $body);
