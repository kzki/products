<?php
/**
 * Album sort page
 *
 * This displays a listing of all the products so that they can be sorted
 */

elgg_gatekeeper();
elgg_group_gatekeeper();

// get the album entity
$album_guid = (int) get_input('guid');
$album = get_entity($album_guid);

// panic if we can't get it
if (!$album) {
    forward();
}

// container should always be set, but just in case
$owner = $album->getContainerEntity();
elgg_set_page_owner_guid($owner->getGUID());

$title = elgg_echo('products:sort', array($album->getTitle()));

// set up breadcrumbs
elgg_push_breadcrumb(elgg_echo('products'), 'products/siteproductsall');
elgg_push_breadcrumb(elgg_echo('products:albums'), 'products/all');
if (elgg_instanceof($owner, 'group')) {
    elgg_push_breadcrumb($owner->name, "products/group/$owner->guid/all");
} else {
    elgg_push_breadcrumb($owner->name, "products/owner/$owner->username");
}
elgg_push_breadcrumb($album->getTitle(), $album->getURL());
elgg_push_breadcrumb(elgg_echo('album:sort'));

if ($album->getSize()) {
    $content = elgg_view_form('products/album/sort', array(), array('album' => $album));
} else {
    $content = elgg_echo('products:sort:no_products');
}

$body = elgg_view_layout('content', array(
    'filter' => false,
    'content' => $content,
    'title' => $title,
    'sidebar' => elgg_view('products/sidebar_al', array('page' => 'upload')),
));

echo elgg_view_page($title, $body);
