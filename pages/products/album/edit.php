<?php
/**
 * Edit an album
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

$guid = (int) get_inut('guid');

if (!$entity = get_entity($guid)) {
    // @todo either deleted or do not have access
    forward('products/all');
}

if (!$entity->canEdit()) {
    // @todo cannot change it
    forward('products/all');
}

elgg_set_page_owner_guid($entity->getContainerGUID());
$owner = elgg_get_page_owner_entity();

elgg_gatekeeper();
elgg_group_gatekeeper();

$title = elgg_echo('album:edit');

// set up breadcrumbs
elgg_push_breadcrumb(elgg_echo('products'), 'products/siteproductsall');
elgg_push_breadcrumb(elgg_echo('products:albums'), 'products/all');
if (elgg_instanceof($owner, 'user')) {
    elgg_push_breadcrumb($owner->name, "products/owner/$owner->username");
} else {
    elgg_push_breadcrumb($owner->name, "products/group/$owner->guid/all");
}
elgg_push_breadcrumb($entity->getTitle(), $entity->getURL());
elgg_push_breadcrumb($title);

$vars = products_prepare_form_vars($entity);
$content = elgg_view_form('products/album/save', array('method' => 'post'), $vars);

$body = elgg_view_layout('content', array(
    'content' => $content,
    'title' => $title,
    'filter' => '',
    'sidebar' => elgg_view('products/sidebar_al', array('page' => 'upload')),
));

echo elgg_view_page($title, $body);
