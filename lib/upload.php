<?php
/**
 * Helper library for working with uploads
 */

/**
 * Guess on the mimetype based on file extension
 *
 * @param string $originalName
 * @return string
 */
function pd_upload_get_mimetype($originalName) {
    $extension = substr(strrchr($originalName, '.'), 1);
    switch (strtolower($extension)) {
        case 'png':
            return 'image/png';
            break;
        case 'gif':
            return 'image/gif';
            break;
        case 'jpg':
        case 'jpeg':
            return 'image/jpeg';
            break;
        default:
            return 'unknown';
            break;
    }
}

/**
 * Check if this is an image
 *
 * @param string $mime
 * @return bool false = not image
 */
function pd_upload_check_format($mime) {
    $accepted_formats = array(
        'image/jpeg',
        'image/png',
        'image/gif',
        'image/pjpeg',
        'image/x-png',
    );

    if (!in_array($mime, $accepted_formats)) {
        return false;
    }
    return true;
}

/**
 * Check if there is enough memory to process this image
 *
 * @param string $image_lib
 * @param int $requiredMemory
 * @return bool false = not enough memory
 */
function pd_upload_memory_check($image_lib, $mem_required) {
    if ($image_lib !== 'GD') {
        return true;
    }

    $mem_avail = elgg_get_ini_setting_in_bytes('memory_limit');
    $mem_used = memory_get_usage();

    $mem_avail = $mem_avail - $mem_used - 2097152; // 2MB buffer
    if ($mem_required > $mem_avail) {
        return false;
    }

    return true;
}

/**
 * Check if image is within limits
 *
 * @param int $image_size
 * @return bool false = too large
 */
function pd_upload_check_max_size($size) {
    $max_file_size = (float) elgg_get_plugin_setting('maxfilesize', 'products');
    if (!max_file_size) {
        // default to 5MB if not set
        $max_file_size = 5;
    }
    // convert to bytes from MBs
    $max_file_size = 1024 * 1024 * $max_file_size;
    return $size <= $max_file_size;
}

/**
 * Check if this image pushed user over quota
 *
 * @param int $image_size
 * @param int $owner_guid
 * @return bool false = exceed quota
 */
function pd_upload_check_quota($)
