<?php
/**
 * Elgg products library of common functions
 */


/**
 * Get products directory path
 *
 * Each album gets a subdirectory based on its container id
 *
 * @return string   path to image directory
 */  
function pd_get_products_dir($album_guid) {
 	$file = new ElggFile();
 	$file->setFilename("products/$album_guid");
 	return $file->getFilenameOnFilestore($file);
}
 
/**
 * Prepare vars for a form, pulling from an entity or sticky forms.
 *
 * @param type $entity
 * @return type
 */
function products_prepare_form_vars($entity = null) {
  // input names => defaults
  $values = array(
    'title' => '',
    'description' => '',
    'access_id' => 'ACCESS_DEFAULT',
    'tags' => '',
    'container_guid' => elgg_get_page_owner_guid(),
    'guid' => null,
    'entity' => $entity,
  );

  if ($entity) {
    foreach (array_keys($values) as $field) {
      if (isset($entity->$field)) {
        $values[$field] = $entity->$field;
      }
    }
  }

  if (elgg_is_sticky_form('products')) {
    $sticky_values = elgg_get_sticky_values('products');
    foreach ($sticky_values as $key => $value) {
      $values[$key] = $value;
    }
  }

  elgg_clear_sticky_form('products');

  return $values;
}
