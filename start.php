<?php
elgg_register_event_handler('init', 'system', 'products_init');

/**
 * 初期化処理
 */
function products_init() {
	// ライブラリを登録する
	$base_dir = elgg_get_plugins_path() . 'products/lib';
	elgg_register_library('products:core', "$base_dir/products.php");
	elgg_load_library('products:core');

	// サイトメニューに追加する
	$item = new ElggMenuItem('products', elgg_echo('products'), 'products/siteproductsall');
	elgg_register_menu_item('site', $item);

	// ページハンドラを登録する
	elgg_register_page_handler('products', 'products_page_handler');

	// CSSを拡張する
	elgg_extend_view('css/elgg', 'products/css');
	elgg_extend_view('css/admin', 'products/css');

	// JavaScriptのライブラリを登録する

	// アルバム選択をajaxで表示できるように登録する。
	elgg_register_ajax_view('products/selectalbum');

	// アクションの登録
	$base_dir = elgg_get_plugins_path() . 'products/actions/products';
	elgg_register_action("products/album/save", "$base_dir/album/save.php");
	elgg_register_action("products/upload/video", "$base_dir/video/save.php");
	elgg_register_action("products/selectalbum", "$base_dir/selectalbum.php");
}

/**
 * 各ページにアクセスした時の処理
 */
function products_page_handler($page) {


	elgg_load_js('lightbox');
	elgg_load_css('lightbox');

	$base = elgg_get_plugins_path() . 'products/pages/products';
	$base_lists = elgg_get_plugins_path() . 'products/pages/lists';

	switch ($page[0]) {
		case "siteproductsall":
			require "$base_lists/siteproductsall.php";
			break;

		case "all": // すべてのアルバム
		case "world":
			require "$base/all.php";
			break;

		case "owned": // albums owned by container entity
		case "owner":
			require "$base/owner.php";
			break;

		case "album": // アルバムを表示する
			set_input('guid', $page[1]);
			require "$base/album/view.php";
			break;

		case "new":  // 新しいアルバムを作る
		case "add":
			set_input('guid', $page[1]);
			require "$base/album/add.php";
			break;

		case "sort": // アルバムの作品をソートする
			set_input('guid', $page[1]);
			require "$base/album/sort.php";
			break;

		case "upload": // アルバムにアップロードする
			$vars['page'] = $page[1];
			set_input('guid', $page[2]);

			if (elgg_get_plugin_setting('uploader', 'products')) {
				$default_uploader = 'ajax';
			} else {
				$default_uploader = 'basic';
			}

			set_input('uploader', elgg_extract(2, $page, $default_uploader));
			require "$base/upload.php";
			break;
	}

	return true;
}

function get_plugload_language() {

	if($current_language = get_current_language()) {
		$path = elgg_get_plugins_path() . "products/vendors/plupload/js/i18n";
		if (file_exists("$path/$current_language.js")) {
			return $current_language;
		}
	}

	return 'en';
}
