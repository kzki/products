<?php
/**
 * Basic uploader form
 *
 * This only handled uploading the images. Editting the titles and descriptions
 * are handled with edit forms.
 *
 * @uses $vars['entity']
 *
 * @auther Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

$album = $vars['entity'];

$maxfilesize = (float) elgg_get_plugin_setting('maxfilesize', 'products');

$instructions = elgg_echo('products:uploader:upload');
$max = elgg_echo('products:uploader:basic', array($maxfilesize));

$list = '';
for ($x = 0; $x < 10; $x++) {
    $list .= '<li>' . elgg_view('input/file', array('name' => 'images[]')) . '</li>';
}

$foot = elgg_view('input/hidden', array('name' => 'guid', 'value' => $album->getGUID()));
$foot .= elgg_view('input/submit', array('value' => elgg_echo("products:addproducts")));

$form_body = <<<HTML
<div>
    $max
</div>
<div>
    <ol>
        $list
    </ol>
</div>
<div class='elgg-foot'>
    $foot
</div>
HTML;

echo elgg_view('input/form', array(
    'body' => $form_body,
    'action' => 'action/products/upload/illust',
    'enctype' => 'multipart/form-data',
));
