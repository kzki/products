<?php
/**
 * フォームを作成する
 */
// ファイル
$file = elgg_echo ('upload:file');
$fileForm = elgg_view ('input/file', array('name' => 'upload'));

// タイトル
$title = elgg_echo ('upload:title');
$titleForm = elgg_view ('input/text', array('name' => 'title'));

// キャプション
$caption = elgg_echo ('upload:caption');
$captionForm = elgg_view ('input/longtext', array('name' => 'caption'));

// タグ
$tag = elgg_echo ('upload:tag');
$tagForm = elgg_view ('input/tags', array('name' => 'tags'));

// 投稿ボタン
$submit = elgg_view('input/submit', array('value' => elgg_echo('upload:submit')));

echo <<<HTML
<div>
    <label>$file<label><br>
    $fileForm
</div>
<div>
    <label>$title</label><br>
    $titleForm
</div>
<div>
    <label>$caption</label><br>
    $captionForm
</div>
<div>
    <label>$tag</label><br>
    $tagForm
</div>
<div>
    $submit
</div>
HTML;
