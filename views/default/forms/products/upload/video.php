<?php
/**
 * save video form body
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

$title = elgg_extract('title', $vars, '');
$description = elgg_extract('description', $vars, '');
$tags = elgg_extract('tags', $vars, '');
$access_id = elgg_extract('access_id', $vars, get_default_access());
$container_guid = elgg_extract('container_guid', $vars, elgg_get_page_owner_guid());
$guid = elgg_extract('guid', $vars, 0);

// 動画URL
$url = "URL";
$urlForm = elgg_view ('input/text', array('name' => 'url'));

// タイトル
$title = elgg_echo ('upload:title');
$titleForm = elgg_view ('input/text', array('name' => 'title'));

// キャプション
$caption = elgg_echo ('upload:caption');
$captionForm = elgg_view ('input/longtext', array('name' => 'description'));

// タグ
$tag = elgg_echo ('upload:tag');
$tagForm = elgg_view ('input/tags', array('name' => 'tags'));

// 投稿ボタン
$submit = elgg_view('input/submit', array('value' => elgg_echo('upload:submit')));

echo <<<HTML
<div>
    <label>$url<label><br>
    $urlForm
</div>
<div>
    <label>$title</label><br>
    $titleForm
</div>
<div>
    <label>$caption</label><br>
    $captionForm
</div>
<div>
    <label>$tag</label><br>
    $tagForm
</div>
<div>
    $submit
</div>
HTML;
