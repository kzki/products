<?php
/**
 * save video form body
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

 $album = $vars['entity'];

// video url
$url_label = "URL";
$url_form = elgg_view ('input/text', array('name' => 'url'));

// title
$title_label = elgg_echo ('title');
$title_form = elgg_view ('input/text', array('name' => 'title'));

// description
$desc_label = elgg_echo ('description');
$desc_form = elgg_view ('input/longtext', array('name' => 'description'));

// tag
$tag_label = elgg_echo ('upload:tag');
$tag_form = elgg_view ('input/tags', array('name' => 'tags'));

// guid, container,
$guid_form = elgg_view('input/hidden', array('name' => 'guid', "value" => $album->getGUID()));

// submit button
$submit = elgg_view('input/submit', array('value' => elgg_echo('save')));


echo <<<HTML
<div>
    <label>$url_label</label><br>
    $url_form
</div>
<div>
    <label>$title_label</label><br>
    $title_form
</div>
<div>
    <label>$desc_label</label><br>
    $desc_form
</div>
<div>
    <label>$tag_label</label><br>
    $tag_form
</div>
<div class="elgg-foot">
    $guid_form
    $submit
</div>
HTML;
