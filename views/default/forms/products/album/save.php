<?php
/**
 * アルバム作成画面のフォーム
 */

// フォームの値を初期化する
$title = elgg_extract('title', $vars, '');
$description = elgg_extract('description', $vars, '');
$tags = elgg_extract('tags', $vars, '');
$access_id = elgg_extract('access_id', $vars, get_default_access());
$container_guid = elgg_extract('container_guid', $vars, elgg_get_page_owner_guid());
$guid = elgg_extract('guid', $vars, 0);

/* ラベルとフォームを作成 */
// タイトル
$title_label = elgg_echo('album:title');
$title_form  = elgg_view('input/text', array('name' => 'title', 'value' => $title));
// キャプション
$desc_label = elgg_echo('album:desc');
$desc_form  = elgg_view('input/longtext', array('name' => 'description', 'value' => $description));
// タグ
$tags_label = elgg_echo('tags');
$tags_form  = elgg_view('input/tags', array('name' => 'tags', 'value' => $tags));
// カテゴリ
$categories = elgg_view('input/categories', $vars);
// 公開範囲
$access_label = elgg_echo('access');
$access_form  = elgg_view('input/access', array('name' => 'access_id', 'value' => $access_id));
// hidden form
$guid_form = elgg_view('input/hidden', array('name' => 'guid', 'value' => $guid));
$container_guid_form = elgg_view('input/hidden', array('name' => 'container_guid', 'value' => $container_guid));
// 保存ボタン
$submit = elgg_view('input/submit', array('value' => elgg_echo('save')));

echo <<<HTML
<div>
	<label>$title_label</label>
	$title_form
</div>
<div>
	<label>$desc_label</label>
	$desc_form
</div>
<div>
	<label>$tags_label</label>
	$tags_form
</div>
$categories
<div>
	<label>$access_label</label>
	$access_form
</div>
<div class="elgg-foot">
$guid_form
$container_guid_form
$submit
</div>
HTML;
