<?php
/**
 * save music form body
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

$title = elgg_extract('title', $vars, '');
$description = elgg_extract('description', $vars, '');
$tags = elgg_extract('tags', $vars, '');
$access_id = elgg_extract('access_id', $vars, get_default_access());
$container_guid = elgg_extract('container_guid', $vars, elgg_get_page_owner_guid());
$guid = elgg_extract('guid', $vars, 0);

// music url
$url_label = "URL";
$url_form = elgg_view ('input/text', array('name' => 'url'));

// file
$file_label = elgg_echo ('file');
$file_form = elgg_view ('input/file', array('name' => 'upload'));

// title
$title_label = elgg_echo ('title');
$title_form = elgg_view ('input/text', array('name' => 'title', 'value' => $title));

// caption
$desc_label = elgg_echo ('description');
$desc_form = elgg_view ('input/longtext', array('name' => 'description', 'value' => $description));

// tag
$tag_label = elgg_echo ('tags');
$tag_form = elgg_view ('input/tags', array('name' => 'tags', 'value' => $tags));

// categories
$categories = elgg_view('input/categories', $vars);

// guid, container,
$guid_form = elgg_view('input/hidden', array('name' => 'guid', 'value' => $guid));
$container_form = elgg_view('input/hidden', array('name' => 'container_guid', 'value' => $container_guid));

// submit button
$submit = elgg_view('input/submit', array('value' => elgg_echo('save')));


echo <<<HTML
<div>
    <label>$url_label<label><br>
    $url_form
</div>
<div>
    <label>$file_label</label><br>
    $file_form
</div>
<div>
    <label>$title_label</label><br>
    $title_form
</div>
<div>
    <label>$desc_label</label><br>
    $desc_form
</div>
<div>
    <label>$tag_label</label><br>
    $tag_form
</div>
$categories
<div class="elgg-foot">
    $guid_form
    $container_form
    $submit
</div>
HTML;
