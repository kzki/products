<?php

if (elgg_is_logged_in()) {
    $base = elgg_get_site_url() . 'products/upload/';

    $tabs = array(
        'illust' => array(
            'title' => elgg_echo('products:illust'),
            'url' => $base . 'illust/' . $vars['album_guid'],
            'selected' => $vars['selected'] == 'illust',
        ),
        'music' => array(
            'title' => elgg_echo('products:music'),
            'url' => $base . 'music/' . $vars['album_guid'],
            'selected' => $vars['selected'] == 'music',
        ),
        'video' => array(
            'title' => elgg_echo('products:video'),
            'url' => $base . 'video/' . $vars['album_guid'],
            'selected' => $vars['selected'] == 'video',
        ),
        'program' => array(
            'title' => elgg_echo ('products:program'),
            'url' => $base . 'program/' . $vars['album_guid'],
            'selected' => $vars['selected'] == 'program',
        ),
        'other' => array(
            'title' => elgg_echo ('products:other'),
            'url' => $base . 'other/' . $vars['album_guid'],
            'selected' => $vars['selected'] == 'other',
        ),
    );

    echo elgg_view('navigation/tabs', array('tabs' => $tabs));
}
