<?php

if (elgg_is_logged_in()) {
	$base = elgg_get_site_url() . 'products/';

	$tabs = array(
		'all' => array(
			'title' => elgg_echo('all'),
			'url' => $base . 'siteproductsall',
			'selected' => $vars['selected'] == 'all',
		),

		'mine' => array(
			'title' => elgg_echo('mine'),
			'url' => $base . 'siteproductsowner',
			'selected' => $vars['selected'] == 'mine',
		),

		'friends' => array(
			'title' => elgg_echo('friends'),
			'url' => $base . 'siteproductsfriends',
			'selected' => $vars['selected'] == 'friends',
		),
	);

	echo elgg_view('navigation/tabs', array('tabs' => $tabs));
}
