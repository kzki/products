<?php
/**
 * SelectAlbum
 */

$owner_guid = get_input('owner_guid', elgg_get_logged_in_user_guid());
$owner = get_entity($owner_guid);
if (!($owner instanceof ElggUser || $owner instanceof ElggGroup)) {
	$owner = elgg_get_logged_in_user_entity();
}

$action = "action/products/selectalbum";

$albums = elgg_get_entities(array('type' => 'object', 'subtype' => 'album', 'container_guid' => $owner->getGUID(), 'limit' => false));

$album_options = array();
$album_options[-1] = elgg_echo('album:create');
if ($albums) {
	foreach ($albums as $album) {
//		$album_title = $album->getTitle();
		$album_title = $album->title;
		if (strlen($album_title) > 50) {
			$album_title = substr($album_title, 0, 47).'...';
		}
		$album_options[$album->guid] = $album_title;
	}
}

$body = "<div style=\"width:400px;\">".elgg_echo('products:album_select')."<br><br>";
$body .= elgg_view('input/hidden', array('name' => 'owner_guid','value' => $owner->guid));
$body .= elgg_view('input/dropdown', array(
	'name' => 'album_guid',
	'value' => '',
	'options_values' => $album_options
));
$body .= "<br><br>";

$body .= elgg_view('input/submit', array('value' => elgg_echo('products:continue'))).'</div>';

echo elgg_view('input/form', array('action' => $action, 'body' => $body));
