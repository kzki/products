<?php
/**
 * Products CSS
 *
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/license/gpl-2.0.html GNU General Public License v2
 */
?>

.elgg-module-products-album,
.elgg-module-products-product {
    width: 161px;
    text-align: left;
    margin: 5px auto;
}
.elgg-module-products-product {
    margin: 5px auto;
}

.products-gallery-widget > li {
    width: 69px;
}
.products-product-wrapper {
    position: relative;
}

.products-headding {
    color: #0054A7;
}
.products-heading:hover {
    color: #0054A7;
    text-decoration: none;
}

.products-input-thin {
    width: 120px;
}

#products-sort li {
    width: 153px;
    height: 153px;
    cursor: move;
}

.products-river-list > li {
    display: inline-block;
}

.products-product-item + .products-product-item {
    margin-left: 7px;
}

.products-gallery > li {
    padding: 0 9px;
}

.products-album-nav {
    margin: 3px 0;
    text-align: center;
    color: #aaa;
}

.products-album-nav > li {
    padding: 0 3px;
}

.products-album-nav > li {
    vertical-align: top;
}

.products-tagging-border1 {
    border: solid 2px white;
}

.products-tagging-border1, .products-tagging-border2,
.tidypics-tagging-border3, .products-tagging-border4 {
    filter: alpha(opacity=50);
    opacity: 0.5;
}

.products-tagging-handle {
    background-color: #fff;
    border: solid 1px #000;
    filter: alpha(opacity=50);
    opacity: 0.5;
}

.products-tagging-outer {
    background-color: #000;
    filter: alpha(opacity=50);
    opacity: 0.5;
}

.products-tagging-help {
    position: absolute;
    left: 35%;
    top: -40px;
    width:450px;
    margin-left: -125px;
    text-align: left;
}

.products-tagging-select {
    position: absolute;
    max-width: 200px;
    text-align: left;
}

.products-tag-wrapper {
    display: none;
    position: absolute;
}

.products-tag {
    border: 2px solid white;
    clear: both;
}

.products-tag-label {
    float: left;
    margin-top: 5px;
    color: #666;
}

#products-uploader {
    position: relative;
    width: 540px;
    min-height: 20px;
}

#uploader {
    text-shadow: none;
}
