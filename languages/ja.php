<?php

return array(
  // Hack for core bug
  'untitled' => "無題",

  // Menu items and titles
  'products' => "作品",
  'products:add' => "アルバム作成",
  'products:albums' => "アルバム",

  'products:add' => "アルバム作成",
  'products:addproducts' => "作品投稿",
  'products:upload' => "このアルバムに追加",

  'products:illust' => "イラスト",
  'products:music' => "音楽",
  'products:video' => "動画",
  'products:program' => "プログラム",
  'products:other' => "その他",

  'album:user' => "%s のアルバム",
  'album:all' => "すべてのアルバム",
  'products:none' => "アルバムに作品が存在しません",
  'products:siteproductsall' => 'すべての作品',
  'products:siteproductsall:nosuccess' => 'まだ一枚も作品が投稿されていません',

  // Actions
  'album:create' => "新規アルバムの作成",
  'album:add' => "アルバムに追加",
  'album:addproducts' => "アルバムに作品を追加する",
  'album:edit' => "アルバムの作成",
  'album:delete' => "アルバムの削除",
  'album:sort' => "ソート",

  // Forms
  'album:title' => "タイトル",
  'album:desc' => "キャプション",
  'album:tags' => "タグ",
  'products:album_select' => "アップロードするアルバムを選んでください。<br>新しくアルバムを作ることもできます。",
  'products:continue' => "続行",
  'products:uploader:basic' => '一度に10枚まで画像をアップロードすることができます。(画像ごとに最大 %s MB) 選択した画像のアップロードが完了したら、あなたがアップロードした画像のタイトルとキャプションを入力することができます。',

  // Albums
  'album:created_by' => 'By ',
  'album:num' => '%s個の作品',

  // Status message
  'album:created' => "あたらしいアルバムが作成されました",

  // Error message
  'album:error' => "アルバム作成に関するエラーが起きました",
  'album:blank' => "アルバムにタイトルをつけてください",
);
