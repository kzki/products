<?php
/**
 * Products Album class
 *
 *
 * @package ProductsAlbum
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */


class ProductsAlbum extends ElggObject {
	/**
	 * Set the internal attributes
	 */
	protected function initializeAttributes() {
		parent::initializeAttributes();

		$this->attributes['subtype'] = "album";
	}

	/**
	 * Constructor
	 * @param mixed $guid
	 */
	public function __construct($guid = null) {
		parent::__construct($guid);
	}

	/**
	 * Save an album
	 *
	 * @return bool
	 */
	public function save() {

		if (!isset($this->new_album)) {
			$this->new_album = true;
		}

		if (!isset($this->last_notified)) {
			$this->last_notified = 0;
		}

		if (!parent::save()) {
			return false;
		}

		mkdir(pd_get_products_dir($this->guid), 0755, true);

		return true;
	}

	/**
	 * Delete album
	 *
	 * @return bool
	 */
	public function delete() {

		$this->deleteProducts();
		$this->deleteAlbumDir();

		return parent::delete();
	}

	/**
	* Get the title of the album
	*
	* @return string
	*/
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Get the URL for this album
	 *
	 * @return string
	 */
	public function getURL() {
		$title = elgg_get_friendly_title($this->getTitle());
		$url = "products/album/$this->guid/$title";
		return elgg_normalize_url($url);
	}
	/**
	 * Get the number of products in the album
	 *
	 * @return int
	 */
	public function getSize() {
		return count($this->getProductList());
	}

	/**
	 * Returns an order list of products guids
	 *
	 * @return array
	 */
	public function getProductList() {
		$listString = $this->orderdProducts;
		if (!$listString) {
			return array();
		}
		$list = unserialize($listString);

		// if empty don't need to check the permissions.
		if (!$list) {
			return array();
		}

		// check access levels
		$guidsString = implode(',', $list);

		$options = array(
			'wheres' => array("e.guid IN ($guidsString)"),
			'order_by' => "FIELD(e.guid, $guidsString)",
			'callback' => 'pd_guid_callback',
			'limit' => ELGG_ENTITIES_NO_VALUE,
		);

		$list = elgg_get_entities($options);
		return $list;
	}

	/**
	 * Delete all the products in this album
	 */
	protected function deleteProducts() {
		$products_count = elgg_get_entities(array(
			"type" => "object",
			"subtype" => array("image", "music", "video", "prog", "other"),
			"container_guid" => $this->guid,
			"count" => true,
		));
		if ($products_count > 0) {
			$products = new ElggBatch('elgg_get_entities', array(
				"type" => "object",
				"subtype" => array("image", "music", "video", "prog", "other"),
				"container_guid" => $this->guid,
				"limit" => ELGG_ENTITIES_NO_VALUE,
			));
			$products->setIncrementOffset(false);
			foreach ($products as $product) {
				if ($product) {
					$product->delete();
				}
			}
		}
	}

	/**
	 * Delete the album directory on disk
	 */
	protected function deleteAlbumDir() {

	}
}
