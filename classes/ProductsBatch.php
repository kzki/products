<?php
/**
 * ProductsBatch class
 */

class ProductsBatch extends ElggObject {

	protected function initializeAttributes() {

		parent::initializeAttributes();

		$this->attributes['subtype'] = "products_batch";
	}

	public function __construct($guid = null) {

		parent::__construct($guid);
	}
}
