<?php
/**
 * Products Video class
 *
 * @package ProductsVideo
 * @author Yoshida Kazuki
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU General Public License v2
 */

class ProductsVideo extends ElggObject {
    protected function initializeAttributes() {
        parent::initializeAttributes();

        $this->attributes['subtype'] = "video";
    }

    public function __construct($guid = null) {
        parent::__construct($guid);
    }

    /**
     * Save the video
     *
     * @return bool
     */
    public function save($data = null) {

        if (!parent::save()) {
            return false;
        }

        return true;
    }

    /**
     * Save the video thumbnails
     */
    protected function saveThumbnails() {
        // @todo
    }

    /**
     * Get the video data of a thumbnail
     */
    public function getThumbnail($size) {
        switch ($size) {
            case 'thumb':
                $thumb = $this->thumbnail;
                break;
            case 'small':
                $thumb = $this->smallthumb;
                break;
            case 'large':
                $thumb = $this->largethumb;
                break;
            default:
                return '';
                break;
        }

        if (!$thumbm) {
            return '';
        }

        $file = new ElggFile();
        $file->owner_guid = $this->getOwnerGUID();
        $file->setFilename($thumb);
        return $file->grabFile();
    }

    /**
     * Get the video id
     *
     * @param $url  example: http://youtube.be/JCVKgcc-qtw
     * @return string  example: /JCVKgcc-qtw
     */
    public function getVideoId($url) {
        $index　= strrpos($url, "/");
        return substr($url, $index);
    }

    /**
     * Get EmbedmentCode
     *
     * @param $id
     * @return string
     */
    public function getEmbedmentCode($id) {
        $url = "https://www.youtube.com/embed" . $id;
        $code = "<iframe width='560' height='315' src='$url' frameborder='0' allowfullscreen></iframe>";
        return $code;
    }

    /**
     * Get the title of the image
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Get the URL for the web page of this video
     *
     * @return string
     */
    public function getURL() {
        $title = elgg_get_friendly_title($this->getTitle());
        $url = "products/video/$this->guid/$title";
        return elgg_normalize_url($url);
    }

    /**
     * Delete video
     *
     * @return bool
     */
    public function delete() {
        @todo
    }

}
